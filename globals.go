package main

// Keyboard ...
type Keyboard map[string]struct {
	code string // key position code, like "B02"
	shif bool   // true = shifted, false = not shifted
}

var qwertyKeyboard = Keyboard{
	"`": {"E00", false},
	"~": {"E00", true},
	"1": {"E01", false},
	"!": {"E01", true},
	"2": {"E02", false},
	"@": {"E02", true},
	"3": {"E03", false},
	"#": {"E03", true},
	"4": {"E04", false},
	"$": {"E04", true},
	"5": {"E05", false},
	"%": {"E05", true},
	"6": {"E06", false},
	"^": {"E06", true},
	"7": {"E07", false},
	"&": {"E07", true},
	"8": {"E08", false},
	"*": {"E08", true},
	"9": {"E09", false},
	"(": {"E09", true},
	"0": {"E10", false},
	")": {"E10", true},
	"-": {"E11", false},
	"_": {"E11", true},
	"=": {"E12", false},
	"+": {"E12", true},
	// TODO...
	// https://docs.google.com/spreadsheets/d/149zWce3w3PPx7o4gsWI8Atw4mz91TP2HCK5aLotk9Wk/edit?ts=5b68bb7d#gid=1065746808
}
