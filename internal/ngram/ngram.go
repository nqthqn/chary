package ngram

import (
	"fmt"
	"regexp"
	"sort"
	"strconv"

	"golang.org/x/text/unicode/runenames"
)

// Gram has the substring that forms an N-Gram and the number of occurances
type Gram struct {
	SubS string
	Occu int
	Perc float32
}

func (g Gram) String() string {
	return fmt.Sprintf("%s\t%d\t%f", Pretty(g.SubS), g.Occu, g.Perc)
	// return fmt.Sprintf("%f", g.Perc)
}

// Ngrams is a map of bigrams, or trigrams, or n-grams
type Ngrams []Gram

// GetGram ...
func (gs Ngrams) GetGram(subs string) Gram {
	for _, g := range gs {
		if g.SubS == subs {
			return g
		}
	}
	return Gram{"", 0, 0}
}

func (gs Ngrams) Len() int { return len(gs) }

// UniqueOccu returns total occurances for all Grams
func (gs Ngrams) UniqueOccu() int {
	c := 0
	for _, g := range gs {
		if g.Occu == 1 {
			c++
		}
	}
	return c
}

// Occurances ...
func (gs Ngrams) Occurances() int {
	c := 0
	for _, g := range gs {
		c += g.Occu
	}
	return c
}
func (gs Ngrams) Less(i, j int) bool { return gs[i].Occu < gs[j].Occu }
func (gs Ngrams) Swap(i, j int)      { gs[i], gs[j] = gs[j], gs[i] }

// PrintSummary ...
func PrintSummary(ngrams Ngrams, size int) {
	count := len(ngrams)
	gramType := ""

	switch size {
	case 1:
		gramType = "monogram"
	case 2:
		gramType = "bigram"
	case 3:
		gramType = "trigram"
	case 4:
		gramType = "quadgram"
	default:
		gramType = strconv.Itoa(size) + "-gram"
	}

	if count != 1 {
		gramType += "s"
	}
	gramType = "# Found " + gramType
	fmt.Println()
	fmt.Printf("%30s: %10d\n", gramType, count)
	fmt.Printf("%30s: %10d\n", "# Unique Occurances", ngrams.UniqueOccu())
}

// PrintGram ...
func PrintGram(gram Gram, points, names bool) {
	if points {
		for _, r := range gram.SubS {
			fmt.Printf(" %U", r)
		}
		fmt.Print("\t")
	}
	fmt.Print(gram, "\t")
	if names {
		for i, r := range gram.SubS {
			fmt.Print(runenames.Name(r))
			if i < len(gram.SubS)-1 {
				fmt.Print(", ")
			}
		}
	}
	fmt.Print("\n")
}

// Counts ...
func Counts(s string, n int, toForm func(string) string) map[string]int {
	rs := []rune(toForm(s))
	counts := make(map[string]int)
	for i := range rs {
		if i+n > len(rs) {
			break
		}
		counts[string(rs[i:i+n])]++
	}
	return counts
}

// Unique ...
func Unique(rs []rune) []rune {
	var sofar []rune
	for _, r := range rs {
		if !contains(sofar, r) {
			sofar = append(sofar, r)
		}
	}
	return sofar
}

func contains(rs []rune, r rune) bool {
	for _, rn := range rs {
		if rn == r {
			return true
		}
	}
	return false
}

func sumVs(m map[string]int) int {
	s := 0
	for _, v := range m {
		s += v
	}
	return s
}

// Pretty ... should go in utils, not in ngrams library stop using regex TODO
func Pretty(s string) string {
	// Replace new lines
	re := regexp.MustCompile(`\r?\n`)
	s = re.ReplaceAllString(s, "␊")

	// Replace tabs
	re2 := regexp.MustCompile(`\t`)
	s = re2.ReplaceAllString(s, "␉")

	// Replace spaces
	re3 := regexp.MustCompile(` `)
	s = re3.ReplaceAllString(s, "␠")
	return s
}

// OrderByOccuranceCount ...
func OrderByOccuranceCount(occurances map[string]int, reverse bool) Ngrams {
	c := len(occurances)
	ngrams := make(Ngrams, c)
	occuTotal := sumVs(occurances)

	i := 0
	for k, v := range occurances {
		ngrams[i] = Gram{k, v, (float32(v) / float32(occuTotal)) * 100}
		i++
	}
	if reverse {
		sort.Sort(ngrams)
	} else {
		sort.Sort(sort.Reverse(ngrams))
	}

	return ngrams
}
