package main

import (
	"errors"
	"flag"
	"fmt"
	"os"

	"golang.org/x/text/unicode/norm"
)

var (
	errFlagParse = errors.New("failed to parse flags")
)

// mainConf ---------
type mainConf struct {
	fs   *flag.FlagSet
	form string
}

func makeMainConf() mainConf {
	c := mainConf{
		fs: flag.NewFlagSet("main", flag.ContinueOnError),
	}

	return c
}

func (c *mainConf) attachFlags() {
	c.fs.StringVar(&c.form, "form", c.form, "Can be NFC or NFD")
}

func (c *mainConf) normalize() error {
	return nil
}

// ngramConf ---------
type ngramConf struct {
	fs       *flag.FlagSet
	file     string
	n        int
	names    bool
	points   bool
	reversed bool
	toForm   func(string) string
}

func makeNgramConf() ngramConf {
	c := ngramConf{
		fs:   flag.NewFlagSet("ngram", flag.ContinueOnError),
		file: "",
	}

	return c
}

func (c *ngramConf) attachFlags() {
	c.fs.StringVar(&c.file, "f", c.file, "file to process")
	c.fs.BoolVar(&c.names, "names", c.names, "print unicode character names")
	c.fs.BoolVar(&c.points, "points", c.points, "print unicode code points")
	c.fs.BoolVar(&c.reversed, "reversed", c.reversed, "reversed ordering of ngrams")
	c.fs.IntVar(&c.n, "n", c.n, "n-gram length (ex: 2 for bigram)")
}

func (c *ngramConf) normalize() error {
	if c.file == "" {
		return fmt.Errorf("file must not be empty string")
	}

	return nil
}

// matrixConf ---------
type matrixConf struct {
	fs     *flag.FlagSet
	file   string
	toForm func(string) string
}

func makeMatrixConf() matrixConf {
	c := matrixConf{
		fs:   flag.NewFlagSet("matrix", flag.ContinueOnError),
		file: "",
	}

	return c
}

func (c *matrixConf) attachFlags() {
	c.fs.StringVar(&c.file, "f", c.file, "file to process")
}

func (c *matrixConf) normalize() error {
	if c.file == "" {
		return fmt.Errorf("file must not be empty string")
	}

	return nil
}

// pitConf ---------
type pitConf struct {
	fs   *flag.FlagSet
	file string
	km1  string
	km2  string
}

func makePitConf() pitConf {
	c := pitConf{
		fs:   flag.NewFlagSet("pit", flag.ContinueOnError),
		file: "",
		km1:  "",
		km2:  "",
	}

	return c
}

func (c *pitConf) attachFlags() {
	c.fs.StringVar(&c.file, "f", c.file, "file to transform using key maps 1 and 2")
	c.fs.StringVar(&c.km1, "km1", c.km1, "json object with keys: unicode code points, values: iso-9995 key positions")
	c.fs.StringVar(&c.km2, "km2", c.km2, "same as km1, internally created hashmap will be reversed (values = keys, keys = values)")
}

func (c *pitConf) normalize() error {
	if c.file == "" {
		return fmt.Errorf("file must not be empty string")
	}
	if c.km1 == "" {
		return fmt.Errorf("km1 must not be empty string")
	}
	if c.km2 == "" {
		return fmt.Errorf("km2 must not be empty string")
	}

	return nil
}

// Conf ... ---------
type Conf struct {
	cmd    string
	main   mainConf
	ngram  ngramConf
	matrix matrixConf
	pit    pitConf
}

func newConf() (*Conf, error) {
	c := &Conf{
		main:   makeMainConf(),
		ngram:  makeNgramConf(),
		matrix: makeMatrixConf(),
		pit:    makePitConf(),
	}

	return c, nil
}

func (c *Conf) parseFlags() error {
	c.main.attachFlags()
	c.ngram.attachFlags()
	c.matrix.attachFlags()
	c.pit.attachFlags()

	if err := c.main.fs.Parse(os.Args[1:]); err != nil {
		return errFlagParse
	}

	if len(c.main.fs.Args()) == 0 {
		return nil
	}

	switch c.cmd = c.main.fs.Args()[0]; c.cmd {
	case c.ngram.fs.Name():
		if err := c.ngram.fs.Parse(nextArgs(os.Args, c.cmd)); err != nil {
			return errFlagParse
		}

		if err := c.ngram.normalize(); err != nil {
			return err
		}
	case c.matrix.fs.Name():
		if err := c.matrix.fs.Parse(nextArgs(os.Args, c.cmd)); err != nil {
			return errFlagParse
		}

		if err := c.matrix.normalize(); err != nil {
			return err
		}
	case c.pit.fs.Name():
		if err := c.pit.fs.Parse(nextArgs(os.Args, c.cmd)); err != nil {
			return errFlagParse
		}

		if err := c.pit.normalize(); err != nil {
			return err
		}
	default:
		fmt.Fprintf(
			c.main.fs.Output(),
			"%q is not a valid subcommand, those available are: [%s]\n",
			c.cmd, c.ngram.fs.Name(),
		)

		return errFlagParse

	}

	return c.main.normalize()
}

func nextArgs(vals []string, val string) []string {
	for k, v := range vals {
		if v == val {
			return vals[k+1:]
		}
	}

	return vals
}

func flood(cnf *Conf) {
	cnf.ngram.toForm = func(s string) string { return s }
	cnf.matrix.toForm = func(s string) string { return s }

	if cnf.main.form == "NFC" {
		cnf.ngram.toForm = norm.NFC.String
		cnf.matrix.toForm = norm.NFC.String
	}

	if cnf.main.form == "NFD" {
		cnf.ngram.toForm = norm.NFD.String
		cnf.matrix.toForm = norm.NFD.String
	}
}
