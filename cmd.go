package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"gitlab.com/nqthqn/chary/internal/ngram"
	"golang.org/x/text/unicode/norm"
)

// ngram ---
func runNgram(cnf ngramConf) error {
	dat, err := ioutil.ReadFile(cnf.file)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	sDat := string(dat)

	ngrams := ngram.OrderByOccuranceCount(ngram.Counts(sDat, cnf.n, cnf.toForm), cnf.reversed)

	for _, gram := range ngrams {
		ngram.PrintGram(gram, cnf.points, cnf.names)
	}

	ngram.PrintSummary(ngrams, cnf.n)

	return nil
}

// matrix ----
func runMatrix(cnf matrixConf) error {
	dat, err := ioutil.ReadFile(cnf.file)
	if err != nil {
		return err
	}
	sDat := ngram.Pretty(string(dat))
	bigrams := ngram.OrderByOccuranceCount(ngram.Counts(sDat, 2, cnf.toForm), true)
	sorted := []rune{}
	for _, g := range bigrams {
		sorted = append(sorted, []rune(g.SubS)...)
	}
	for _, r := range []rune(sDat) {
		sorted = append(sorted, r)
	}

	uniqrs := ngram.Unique([]rune(sorted))

	matrix := make([][]int, len(uniqrs))
	for i, a := range uniqrs {
		matrix[i] = make([]int, len(uniqrs))
		for j, b := range uniqrs {
			s := string(a) + string(b)
			g := bigrams.GetGram(s)
			matrix[i][j] = 0
			if g.Occu != 0 {
				matrix[i][j] = g.Occu
			}
		}
	}
	// Sort the matrix

	fmt.Print(".\t")
	for _, c := range uniqrs {
		fmt.Print(string(c))
		fmt.Print("\t")
	}
	fmt.Println()
	for i, row := range matrix {
		fmt.Print(string(uniqrs[i]))
		fmt.Print("\t")
		for _, o := range row {
			fmt.Print(o)
			fmt.Print("\t")
		}
		fmt.Println()
	}

	return nil
}

func toMap(jsonFileName string) (map[string]string, error) {
	km := make(map[string]string)
	dat, err := ioutil.ReadFile(jsonFileName)
	if err != nil {
		return nil, fmt.Errorf("failed to open km1 file: %v", err)
	}
	if err := json.Unmarshal(dat, &km); err != nil {
		return nil, fmt.Errorf("could not unmarshal json from km1: %v", err)
	}
	return km, nil
}

// runPit ----
func runPit(cnf pitConf) error {

	km1 := make(map[rune]string)
	km2 := make(map[string]rune)

	// Open km1 json file, transform to hash table: rune -> iso9995 key code
	kms1, err := toMap(cnf.km1)
	if err != nil {
		return err
	}
	for k, v := range kms1 {
		n, err := strconv.ParseUint(k, 16, 32)
		if err != nil {
			return fmt.Errorf("could not decode hex: %v", err)
		}
		km1[rune(n)] = v
	}

	// Open km2 json file, transform to hash table: iso9995 key code -> rune
	kms2, err := toMap(cnf.km2)
	if err != nil {
		return err
	}
	for k, v := range kms2 {
		n, err := strconv.ParseUint(k, 16, 32)
		if err != nil {
			return fmt.Errorf("could not decode hex: %v", err)
		}
		km2[v] = rune(n)
	}

	// Output the corpus as if it were typed on a km2 keyboard layout
	f, err := os.Open(cnf.file)
	if err != nil {
		return fmt.Errorf("failed to open file: %v", err)
	}
	input := bufio.NewScanner(f)
	for input.Scan() {
		for _, r := range norm.NFD.String(input.Text()) {
			code, prs := km1[r]
			if !prs {
				return fmt.Errorf("could not find %U in km1", r)
			}
			char, prs := km2[code]
			if !prs {
				return fmt.Errorf("could not find %v in km1", code)
			}
			fmt.Print(string(char))
		}
	}
	return nil
}

// Sub-commands ---
func runCommand(c *Conf) error {
	switch c.cmd {
	case c.ngram.fs.Name():
		return runNgram(c.ngram)
	case c.matrix.fs.Name():
		return runMatrix(c.matrix)
	case c.pit.fs.Name():
		return runPit(c.pit)
	default:
		return fmt.Errorf("missing/unknown command")
	}
}
